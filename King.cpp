#include "King.h"
#include "Chess.h"

bool King::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  //Check up/down/left/right and diagonals.
  if (end.first == start.first + 1 && end.second == start.second) { //Right
    return true;
  } else if (end.first == start.first - 1 && end.second == start.second) { //Left
    return true;
  } else if (end.first == start.first && end.second == start.second + 1) { //Up
    return true;
  } else if (end.first == start.first && end.second == start.second - 1) { //Down
    return true;
  } else if (end.first == start.first + 1 && end.second == start.second + 1) { //Right Up
    return true;
  } else if (end.first == start.first - 1 && end.second == start.second - 1) { //Right Down
    return true;
  } else if (end.first == start.first - 1 && end.second == start.second + 1) { //Left Up
    return true;
  } else if (end.first == start.first + 1 && end.second == start.second - 1) { //Left Down
    return true;
  }
  return false;
}

bool King::legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  if (legal_move_shape(start, end)) {
    return true;
  } else {
    return false;
  }
}
