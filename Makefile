CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)


main: main.o Chess.o Board.o CreatePiece.o King.o Pawn.o Knight.o Bishop.o Rook.o Queen.o moveshape.o
	g++ -o main main.o Chess.o Board.o CreatePiece.o King.o Pawn.o Knight.o Bishop.o Rook.o Queen.o moveshape.o

main.o: main.cpp Chess.h
	g++ -c main.cpp $(CFLAGS)

Board.o: Board.cpp Board.h CreatePiece.h
	g++ -c Board.cpp $(CFLAGS)

Chess.o: Chess.cpp Chess.h
	g++ -c Chess.cpp $(CFLAGS)

CreatePiece.o: CreatePiece.cpp CreatePiece.h
	g++ -c CreatePiece.cpp $(CFLAGS)

King.o: King.cpp King.h
	g++ -c King.cpp $(CFLAGS)

Pawn.o: Pawn.cpp Pawn.h
	g++ -c Pawn.cpp $(FLAGS)

Knight.o: Knight.cpp Knight.h
	g++ -c Knight.cpp $(FLAGS)

Bishop.o: Bishop.cpp Bishop.h moveshape.h
	g++ -c Bishop.cpp $(FLAGS)

Rook.o: Rook.cpp Rook.h moveshape.h
	g++ -c Rook.cpp $(FLAGS)

Queen.o: Queen.cpp Queen.h moveshape.h
	g++ -c Queen.cpp $(FLAGS)

moveshape.o: moveshape.cpp moveshape.h
	g++ -c moveshape.cpp $(FLAGS)

.PHONY: clean
clean:
	rm -f *.o *~ main
