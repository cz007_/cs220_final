#include "Chess.h"
#include <iostream>
#include <cstdlib>
#include <algorithm>

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess() : is_white_turn(true) {
	// Add the pawns
	for (int i = 0; i < 8; i++) {
		board.add_piece(std::pair<char, char>('A' + i, '1' + 1), 'P');
		board.add_piece(std::pair<char, char>('A' + i, '1' + 6), 'p');
	}

	// Add the rooks
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+7 ) , 'r' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+7 ) , 'n' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+7 ) , 'b' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+0 ) , 'Q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+0 ) , 'K' );
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+7 ) , 'q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+7 ) , 'k' );
}

//Checks if the move can be made.
bool Chess::make_move(std::pair<char, char> start,
		      std::pair<char, char> end) {
  //Checks if there is a piece at start.
  //If no piece exists at the start, so the move is invalid.
  std::pair<char, char> king_pos, cur_pos;
  bool check_break = false;
  
  if (turn_white()) {
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	cur_pos = std::make_pair(c,r);
	if (board(cur_pos) != NULL) {
	  if (board(cur_pos)->to_ascii() == 'K') {
	    king_pos = cur_pos;
	    break;
	  }
	}
      }
    }
  }
  else { //Black king check.
    //Double for loop to find the king's position.
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	cur_pos = std::make_pair(c,r);
	if (board(cur_pos) != NULL) {
	  if (board(cur_pos)->to_ascii() == 'k') {
	    king_pos = cur_pos;
	    break;
	  }
	}
      }
    }
  }
  
  if (board(start) == NULL) {
    std::cerr << "Error: No piece at starting square" << std::endl;
    return false;
  }
  
  if (!turn_white() && board(start)->is_white()) {
    std::cerr << "Error: Wrong piece color" << std::endl;
    return false;
  } else if (turn_white() && !(board(start)->is_white())) {
    std::cerr << "Error: Wrong piece color" << std::endl;
    return false;
  } 
  //Checks if the move shape is legal for the respective start
  //and end positions.
  if (board(end) != NULL) {
    if (board(start)->is_white() && board(end)->is_white()) {
      std::cerr << "Error: Cannot capture own piece" << std::endl;
      return false;
    } else if (!board(start)->is_white() && !(board(end)->is_white())) {
      std::cerr << "Error: Cannot capture own piece" << std::endl;
      return false;
    }
  }
  
  const Piece *cur_piece = board(start);
  if (board(start)->to_ascii() == 'P' || board(start)->to_ascii() == 'p') {
    if (!(cur_piece->legal_move_shape(start, end)) && !(cur_piece->legal_capture_shape(start, end))) {
    //std::cout << '1' << std::endl;
      if (board(end) == NULL && (cur_piece->legal_capture_shape(start, end))) {
	std::cerr << "Pawn cannot capture when no piece at end." << std::endl;
	return false;
      }
      /*
      if(!cur_piece->legal_move_shape(start, end))
	std::cout << "1" << std::endl;
      if(!cur_piece->legal_capture_shape(start, end))
      std::cout << '2' << std::endl;*/
    std::cerr << "Error: Move is not legal." << std::endl;
    return false;
    }
  }
  else if (!(cur_piece->legal_move_shape(start, end))) {
    //std::cout << '1' << std::endl;
    std::cerr << "Error: Move is not legal." << std::endl;
    return false;
  }

  //Calls helper function and it returns a vectors of positions where pieces
  //occupy that includes the start but not the end position of the move. End
  //position is checked in another conditional after.
  if (start.first == end.first) {
    if (end.second > start.second) {
      for (char i = start.second + 1; i < end.second; i++) {
	std::pair<char, char> cur_pos = std::make_pair(start.first, i);
	if (board(cur_pos) != NULL) {
	  //std::cout << board(cur_pos)->to_ascii() << std::endl;
	  std::cerr << "Error: Piece in the way of move." << std::endl;
	  return false;
	}
      }
    } else if (end.second < start.second) {
      for (char i = start.second - 1; i > end.second; i--) {
	std::pair<char, char> cur_pos = std::make_pair(start.first, i);
	if (board(cur_pos) != NULL) {
	  //std::cout << '2' << std::endl;
	  std::cerr << "Error: Piece in the way of move." << std::endl;
	  return false;
	}
      }
    } else {
      std::cerr << "Error: Starting and ending squares are the same." << std::endl;
      return false;
    }
  } else if (start.second == end.second) {
    if (start.first < end.first) {
      for (char i = start.first + 1; i < end.first; i++) {
	std::pair<char, char> cur_pos = std::make_pair(i, start.second);
	if (board(cur_pos) != NULL) {
	  std::cerr << "Error: Piece in the way of move." << std::endl;
	  return false;
	}
      }
    } else if (start.first > end.first) {
      for (char i = start.first - 1; i > end.first; i--) {
	std::pair<char, char> cur_pos = std::make_pair(i, start.second);
	if (board(cur_pos) != NULL) {
	  std::cerr << "Error: Piece in the way of move." << std::endl;
	  return false;
	}
      }
    } else {
      std::cerr << "Error: Starting and ending squares are the same." << std::endl;
      return false;
    }
  } else if (start.first == end.first && start.second == end.second){
    std::cerr << "Error: Starting and ending squares are the same." << std::endl;
    return false;
  } else if (abs(start.first - end.first) == abs(start.second - end.second)) {
    if (end.first > start.first && end.second > start.second) {
      for (char i = start.first + 1, j = start.second + 1; i < end.first || j < end.second; i++, j++) {
	std::pair<char, char> cur_pos = std::make_pair(i, j);
	if (board(cur_pos) != NULL) {
	  std::cerr << "Error: Piece in the way of move." << std::endl;
	  return false;
	}
      }
    } else if (end.first > start.first && end.second < start.second) {
      for (char i = start.first + 1, j = start.second - 1; i < end.first || j > end.second; i++, j--) {
	std::pair<char, char> cur_pos = std::make_pair(i, j);
	if (board(cur_pos) != NULL) {
	  std::cerr << "Error: Piece in the way of move." << std::endl;
	  return false;
	}
       }
    } else if (end.first < start.first && end.second > start.second) {
      for (char i = start.first - 1, j = start.second + 1; i > end.first || j < end.second; i--, j++) {
	std::pair<char, char> cur_pos = std::make_pair(i, j);
	if (board(cur_pos) != NULL) {
	  std::cerr << "Error: Piece in the way of move." << std::endl;
	  return false;
	}
       }
    } else if (end.first < start.first && end.second < start.second) {
      for (char i = start.first - 1, j = start.second - 1; i > end.first || j > end.second; i--, j--) {
	std::pair<char, char> cur_pos = std::make_pair(i, j);
	if (board(cur_pos) != NULL) {
	  std::cerr << "Error: Piece in the way of move." << std::endl;
	  return false;
	}
      }
    }
  }
  
  if(in_check(turn_white())){
    std::vector<std::pair<char, char>> v, free;
    std::vector<std::pair<char, char>> d;
    
    if(start == king_pos){
      for(int i = king_pos.first - 1; i <= king_pos.first + 1; i++){
	for(int j = king_pos.second - 1; j <= king_pos.second + 1; j++){
	  if (i < 73 && i > 64 && j > 48 && j < 57){
	    std::pair<char, char> P = std::make_pair((char)i,(char)j);
	    if(board(P) == NULL || (board(P)->is_white() != turn_white()))
	      v.push_back(P);
	  }
	}
      }
      for(std::vector<std::pair<char, char>>::iterator it = v.begin(); it != v.end(); ++it){
	d = in_danger(*it, turn_white());
        if(d.empty()){
	  free.push_back(*it);
	}
      }

      std::cout<< v.size() << std::endl << free.size() << std::endl;
      
      if(!free.empty()){
	for(std::vector<std::pair<char, char>>::iterator it = free.begin(); it != free.end(); ++it){
	  if(end == *it){
	    // Confirms piece can be moved to cancel check
	    //std::cout << "bruh moment #1" << std::endl;
	    check_break = true;
	  }
	}
      }
    }
    if(check_break == false){
      std::vector<std::pair<char, char>> a, atk_path;
      a = in_danger(king_pos, turn_white());
      std::vector<std::pair<char, char>> c_a, b;
      c_a = counter_attackers(a[0]);
      b = blockers(a[0]);
      
      if(!c_a.empty()){
	for(std::vector<std::pair<char, char>>::iterator it = c_a.begin(); it != c_a.end(); ++it){
	  if(start == *it && end == a[0] && check_move(*it, a[0])){
	    check_break = true;
	    //std::cout << "bruh moment #2" << std::endl;
	  }
	}
      }
      
      if(!b.empty()){
	for(std::vector<std::pair<char, char>>::iterator it = b.begin(); it != b.end(); ++it){
	  atk_path = path(a[0], king_pos);
	  for(std::vector<std::pair<char, char>>::iterator it2 = atk_path.begin(); it2 != atk_path.end(); ++it2){
	    if(start == *it && end == *it2 && check_move(*it, *it2)){
	      check_break = true;
	    }
	  }
	}
      }
    }
    if(check_break == false){
      std::cerr << "Move must break check" << std::endl;
      return false;
    }
  }
  
  //Checks if there is a piece at the end where we are going to.
  //If there is no piece here, check legal move.
  //Otherwise, check legal capture.
  if (board(end) != NULL) {
    if (!(cur_piece->legal_capture_shape(start, end))) {
      std::cout << "Piece not in capture shape" << std::endl;
      return false;
    } else {
      board.clear_piece(end);
      char val = cur_piece->to_ascii();
      board.add_piece(end, val);
      board.clear_piece(start);
      // Promotes pawn to queen
      if(val == 'P' && end.second == '8'){
	board.clear_piece(end);
	board.add_piece(end,'Q');
      }
      else if(val == 'p' && end.second == '1'){
	board.clear_piece(end);
	board.add_piece(end,'q');
      }
      is_white_turn = !(is_white_turn);
      return true;
    }
  } else {
    //Calls piece's legal move shape
    char val = cur_piece->to_ascii();
    board.add_piece(end, val);
    board.clear_piece(start);
    if (val == 'P' && end.second == '8'){
      board.clear_piece(end);
	board.add_piece(end,'Q');
    } else if (val == 'p' && end.second == '1'){
      board.clear_piece(end);
      board.add_piece(end,'q');
    }
    is_white_turn = !(is_white_turn);
    return true;
  }
  is_white_turn = !(is_white_turn);
  return true;
}

// Checks if move is possible but does not make it
bool Chess::check_move(std::pair<char, char> start,
		      std::pair<char, char> end) const {
  //Checks if there is a piece at start.
  //If no piece exists at the start, so the move is invalid.
  if (board(start) == NULL) {
    return false;
  }
  
  //Checks if the move shape is legal for the respective start
  //and end positions.
  const Piece *cur_piece = board(start);
  if (!(cur_piece->legal_move_shape(start, end)) && !(cur_piece->legal_capture_shape(start, end))) {
    return false;
  }

  //Calls helper function and it returns a vectors of positions where pieces
  //occupy that includes the start but not the end position of the move. End
  //position is checked in another conditional after.
  if (start.first == end.first) {
    if (end.second > start.second) {
      for (char i = start.second + 1; i < end.second; i++) {
	std::pair<char, char> cur_pos = std::make_pair(start.first, i);
	if (board(cur_pos) != NULL && 
	    ((turn_white() && (board(cur_pos)->to_ascii() == 'k')) ||
	     (!turn_white() && (board(cur_pos)->to_ascii() == 'K')))) {
	  return false;
	}
      }
    } else if (end.second < start.second) {
      for (char i = start.second - 1; i > end.second; i--) {
	std::pair<char, char> cur_pos = std::make_pair(start.first, i);
	if (board(cur_pos) != NULL &&
	    ((turn_white() && (board(cur_pos)->to_ascii() == 'k')) ||
	     (!turn_white() && (board(cur_pos)->to_ascii() == 'K')))) {
	  return false;
	}
      }
    } else {
      return false;
    }
  } else if (start.second == end.second) {
    if (start.first < end.first) {
      for (char i = start.first + 1; i < end.first; i++) {
	std::pair<char, char> cur_pos = std::make_pair(i, start.second);
	if (board(cur_pos) != NULL &&
	    ((turn_white() && (board(cur_pos)->to_ascii() == 'k')) ||
	     (!turn_white() && (board(cur_pos)->to_ascii() == 'K')))) {
	  return false;
	}
      }
    } else if (start.first > end.first) {
      for (char i = start.first - 1; i > end.first; i--) {
	std::pair<char, char> cur_pos = std::make_pair(i, start.second);
	if (board(cur_pos) != NULL &&
	    ((turn_white() && (board(cur_pos)->to_ascii() == 'k')) ||
	     (!turn_white() && (board(cur_pos)->to_ascii() == 'K')))) {
	  return false;
	}
      }
    } else {
      return false;
    }
  } else if (start.first == end.first && start.second == end.second){
    return false;
  } else if (abs(start.first - end.first) == abs(start.second - end.second)) {
    if (end.first > start.first && end.second > start.second) {
      for (char i = start.first + 1, j = start.second + 1; i < end.first || j < end.second; i++, j++) {
	//std::cout << "here\n";
	std::pair<char, char> cur_pos = std::make_pair(i, j);
	if (board(cur_pos) != NULL &&
	    ((turn_white() && (board(cur_pos)->to_ascii() == 'k')) ||
	     (!turn_white() && (board(cur_pos)->to_ascii() == 'K')))) {
	  return false;
	}
      }
    } else if (end.first > start.first && end.second < start.second) {
      for (char i = start.first + 1, j = start.second - 1; i < end.first || j > end.second; i++, j--) {
	std::pair<char, char> cur_pos = std::make_pair(i, j);
	if (board(cur_pos) != NULL &&
	    ((turn_white() && (board(cur_pos)->to_ascii() == 'k')) ||
	     (!turn_white() && (board(cur_pos)->to_ascii() == 'K')))) {
	  return false;
	}
       }
    } else if (end.first < start.first && end.second > start.second) {
      for (char i = start.first - 1, j = start.second + 1; i > end.first || j < end.second; i--, j++) {
	std::pair<char, char> cur_pos = std::make_pair(i, j);
	if (board(cur_pos) != NULL &&
	    ((turn_white() && (board(cur_pos)->to_ascii() == 'k')) ||
	     (!turn_white() && (board(cur_pos)->to_ascii() == 'K')))) {
	  return false;
	}
       }
    } else if (end.first < start.first && end.second < start.second) {
      for (char i = start.first - 1, j = start.second - 1; i > end.first || j > end.second; i--, j--) {
	std::pair<char, char> cur_pos = std::make_pair(i, j);
	if (board(cur_pos) != NULL &&
	    ((turn_white() && (board(cur_pos)->to_ascii() == 'k')) ||
	     (!turn_white() && (board(cur_pos)->to_ascii() == 'K')))) {
	  return false;
	}
      }
    }
  }
  
  //Checks if there is a piece at the end where we are going to.
  //If there is no piece here, check legal move.
  //Otherwise, check legal capture.
  if (board(end) != NULL) {
    if (!(cur_piece->legal_capture_shape(start, end))) {
      return false;
    } else{ 
      return true;
    }
  } else { 
      return true;
  }
  return true;
}

//In check boolean.
bool Chess::in_check(bool white) const {
  //Initializes variables.
  std::pair<char, char> king_pos;
  std::vector<std::pair<char, char>> check_list, spaces;
  std::pair<char, char> cur_pos;
  
  if (white) {
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	cur_pos = std::make_pair(c,r);
	if (board(cur_pos) != NULL) {
	  if (board(cur_pos)->to_ascii() == 'K') {
	    king_pos = cur_pos;
	    break;
	  }
	}
      }
    }
    check_list = this->in_danger(king_pos, white);
  }
  else { //Black king check.
    //Double for loop to find the king's position.
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	cur_pos = std::make_pair(c,r);
	if (board(cur_pos) != NULL) {
	  if (board(cur_pos)->to_ascii() == 'k') {
	    king_pos = cur_pos;
	    break;
	  }
	}
      }
    }
    check_list = this->in_danger(king_pos, white);
  }
  if (!(check_list.empty())) {
    return true;
  }
  return false;
}




//Returns list of attackers.
std::vector<std::pair<char, char>> Chess::in_danger(std::pair<char, char> p, bool white) const{
  //Initializes variables.
  std::pair<char, char> king_pos = p;
  std::vector<std::pair<char, char>> check_list;
  std::pair<char, char> cur_pos;
  
    //Finds the Piece pointer.
  if(white){
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	cur_pos = std::make_pair(c,r);
	const Piece *cur_piece = board(cur_pos);
	if (cur_piece != NULL) {
	  if (!(cur_piece->is_white()) && check_move(cur_pos, king_pos)) {
	    check_list.push_back(cur_pos);
	  }
	}
      }
    }
  } 
     //Double for loop to see if the current piece can capture the king
     //legally, if yes, add it to the check_list positions.
  if(!white){
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	cur_pos = std::make_pair(c,r);
	//const Piece *cur_piece = board(cur_pos);
	if (board(cur_pos) != NULL) {
	  if (board(cur_pos)->is_white() && check_move(cur_pos, king_pos)) {
	    check_list.push_back(cur_pos);
	  }
	}
      }
    }
  }
  return check_list;
}


bool Chess::in_mate(bool white) const {
  std::pair<char, char> king_p;
  std::pair<char, char> p, cur_pos;
  std::vector<std::pair<char,char>> v;
  std::vector<std::pair<char, char>> check_list;
  std::vector<std::pair<char, char>> spaces;
  
  if (white && in_check(white)) {
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if (board(p) != NULL) {
	  if(board(p)->to_ascii() == 'K'){
	    king_p = p;
	    break;
	  }
	}
      }
    }
    // Checks if King can move out of Check
    for(char i = king_p.first - 1; i <= king_p.first + 1; i++){
      for(char j = king_p.second - 1; j <= king_p.second + 1; j++){
	if (i < 73 && i > 64 && j > 48 && j < 57){
	  std::pair<char, char> P = std::make_pair(i,j);
	  if(board(P) == NULL || !(board(P)->is_white()))
	    v.push_back(P);
	}
      }
    }
    //std::cout << v.size() << std::endl;
    
    for(std::vector<std::pair<char, char>>::iterator it = v.begin();
	it != v.end();
	++it){
      std::vector<std::pair<char, char>> check_list_iter = in_danger(*it, white);
      if(check_list_iter.empty()){
	//std::cerr << "Square available" << std::endl;
	//std::cout << check_list_iter.size() << std::endl;
	return false;
      }
    }
    check_list = in_danger(king_p, white);
    if (check_list.size() > 1)
      return true;
    else if (check_list.size() == 1){
      std::pair<char, char> attacker = check_list[0];
      for (char r = '8'; r >= '1'; r--) {
	for (char c = 'A'; c <= 'H'; c++) {
	  p = std::make_pair(c,r);
	  if(board(p) != NULL && (board(p)->is_white()) && (std::find(v.begin(), v.end(), p) != v.end())){
	    if(check_move(p, attacker)){
	      //std::cout << "Counter attackers exist." << std::endl;
	      return false;
	    }
	  }
	}
      }
      spaces = path(check_list[0], king_p);
      if(spaces.empty()){
	return true;
      }
      //std::cerr << "" << std::endl;
      return !(this->block(spaces, white));
    }
  }
  else if(!(white) && in_check(white)){
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if (board(p) != NULL) {
	  if(board(p)->to_ascii() == 'k'){
	    king_p = p;
	    break;
	  }
	}
      }
    }
    // Checks if King can move out of Check
    for(char i = king_p.first - 1; i <= king_p.first + 1; i++){
      for(char j = king_p.second - 1; j <= king_p.second + 1; j++){
	if (i < 73 && i > 64 && j > 48 && j < 57){
	  std::pair<char, char> P = std::make_pair(i,j);
	  if(board(P) == NULL || (board(P)->is_white()))
	    v.push_back(P);
	}
      }
    }
    //std::cout << v.size() <<  std::endl;

    for(std::vector<std::pair<char, char>>::iterator it = v.begin();
	it != v.end();
	++it){
      std::vector<std::pair<char, char>> check_list_iter = in_danger(*it, white);
      if(check_list_iter.empty()){
	//std::cerr << "No enemy pieces threatening this square" << std::endl;
	return false;
      }
    }

    // Checks if 
    check_list = in_danger(king_p, white);
    if (check_list.size() > 1)
      return true;
    else if (check_list.size() == 1){
      std::pair<char, char> attacker = check_list[0];
      for (char r = '8'; r >= '1'; r--) {
	for (char c = 'A'; c <= 'H'; c++) {
	  p = std::make_pair(c,r);
	  if(board(p) != NULL && !(board(p)->is_white()) && (std::find(v.begin(), v.end(), p) != v.end())){
	    if(check_move(p, attacker)){
	      //std::cerr << "No enemy pieces threatening this square1234" << std::endl;
	      return false;
	    }
	  }
	}
      }
      spaces = path(check_list[0], king_p);
      if(spaces.empty()){
	return true;
      }
      //std::cerr << "No enemy pieces threatening this square567" << std::endl;
      return !(this->block(spaces, white));
    }
  }
  //std::cerr << "No enemy pieces threatening this square678" << std::endl;
  return false;
  /*
  if (white && in_check(white)) {
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if (board(p) != NULL) {
	  if(board(p)->to_ascii() == 'K'){
	    king_pos = p;
	    break;
	  }
	}
      }
    }
  }else if(!(white) && in_check(white)) {
      for (char r = '8'; r >= '1'; r--) {
	for (char c = 'A'; c <= 'H'; c++) {
	  p = std::make_pair(c,r);
	  if (board(p) != NULL) {
	    if(board(p)->to_ascii() == 'k'){
	      king_pos = p;
	      break;
	    }
	  }
	}
      }
  } else {
    //std::cerr << "King not in check, so not in mate." << std::endl;
    return false;
  }
  if(in_check(turn_white())) {
    std::vector<std::pair<char, char>> v, free;
    std::vector<std::pair<char, char>> d;
    for(int i = king_pos.first - 1; i <= king_pos.first + 1; i++){
      for(int j = king_pos.second - 1; j <= king_pos.second + 1; j++){
	if (i < 73 && i > 64 && j > 48 && j < 57){
	  std::pair<char, char> P = std::make_pair((char)i,(char)j);
	  if(board(P) == NULL || (board(P)->is_white())
	    v.push_back(P);
	}
      }
      for(std::vector<std::pair<char, char>>::iterator it = v.begin(); it != v.end(); ++it){
	d = in_danger(*it, turn_white());
        if(d.empty()){
	  std::cerr << "empty squares" << std::endl;
	  return false;
	}
      }
    }
    std::vector<std::pair<char, char>> a, atk_path;
    a = in_danger(king_pos, turn_white());
    std::vector<std::pair<char, char>> c_a, b;
    c_a = counter_attackers(a[0]);
    b = blockers(a[0]);
    if (c_a.empty() && b.empty()) {
      return true;
    }
  }
  std::cerr << "Default false." << std::endl;
  return false;*/
}

std::vector<std::pair<char, char>> Chess::counter_attackers(std::pair<char, char> a) const{
  std::pair<char, char> p;
  std::vector<std::pair<char, char>> counter_attackers;
  if(turn_white()){
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if(board(p) != NULL && (board(p)->is_white())){
	  if(check_move(p, a)){
	    counter_attackers.push_back(p);
	  }
	}
      }
    }
  }
  else{
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if(board(p) != NULL && !(board(p)->is_white())){
	  if(check_move(p, a)){
	    counter_attackers.push_back(p);
	  }
	}
      }
    }
  }
  return counter_attackers;
}

std::vector<std::pair<char, char>> Chess::blockers(std::pair<char, char> a) const{
  std::pair<char, char> p, k;
  std::vector<std::pair<char, char>> blockers, path_;
  
  if(turn_white()){
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if(board(p) != NULL && board(p)->to_ascii() == 'K'){
	  k = p;
	  break;
	}
      }
    }
    path_ = path(a, k);
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if(board(p) != NULL && board(p)->is_white()){
	  for(std::vector<std::pair<char,char>>::iterator it = path_.begin();
	      it != path_.end();
	      ++it){
	    if(check_move(p, *it)){
	      blockers.push_back(p);
	    }
	  }
	}
      }
    }
  }
  else{
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if(board(p) != NULL && board(p)->to_ascii() == 'k'){
	  k = p;
	  break;
	}
      }
    }
    path_ = path(a, k);
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if(board(p) != NULL && !(board(p)->is_white())){
	  for(std::vector<std::pair<char,char>>::iterator it = path_.begin();
	      it != path_.end();
	      ++it){
	    if(check_move(p, *it)){
	      blockers.push_back(p);
	    }
	  }
	}
      }
    }
  }
  return blockers;
}



//Block function.
bool Chess::block(std::vector<std::pair<char, char>> path, bool white) const{
  std::pair<char, char> p;
  if (white) {
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if(board(p) != NULL && board(p)->is_white()){
	  for(std::vector<std::pair<char,char>>::iterator it = path.begin(); it != path.end(); ++it){
	    if(check_move(p, *it))
	      return false;
	  }
	}
      }
    }
  } else {
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if(board(p) != NULL && !(board(p)->is_white())){
	  for(std::vector<std::pair<char,char>>::iterator it = path.begin(); it != path.end(); ++it){
	    if(check_move(p, *it))
	      return false;
	  }
	}
      }
    }
  }
  return false;
}

//Returns path of attacker
std::vector<std::pair<char, char>> Chess::path(std::pair<char, char> a /* attacker */, std::pair<char, char> k /*king*/) const{
  std::vector<std::pair<char, char>> path;
  //const Piece *cur_piece = board(a);

  //Calls helper function and it returns a vectors of positions where pieces
  //occupy that includes the start but not the end position of the move. End
  //position is checked in another conditional after.
  if (a.first == k.first) {
    if (k.second > a.second) {
      for (char i = a.second + 1; i < k.second; i++) {
	std::pair<char, char> cur_pos = std::make_pair(a.first, i);
	path.push_back(cur_pos);
      }
    } else if (k.second < a.second) {
      for (char i = a.second - 1; i > k.second; i--) {
	std::pair<char, char> cur_pos = std::make_pair(a.first, i);
	path.push_back(cur_pos);

      }
    }
  } else if (a.second == k.second) {
    if (a.first < k.first) {
      for (char i = a.first + 1; i < k.first; i++) {
	std::pair<char, char> cur_pos = std::make_pair(i, a.second);
     	path.push_back(cur_pos);
      }
    } else if (a.first > k.first) {
      for (char i = a.first - 1; i > k.first; i--) {
	std::pair<char, char> cur_pos = std::make_pair(i, a.second);
	path.push_back(cur_pos);
      }
    }
  }  else if (abs(a.first - k.first) == abs(a.second - k.second)) {
    if (k.first > a.first && k.second > a.second) {
      for (char i = a.first + 1, j = a.second + 1; i < k.first || j < k.second; i++, j++) {
	//std::cout << "here\n";
	std::pair<char, char> cur_pos = std::make_pair(i, j);
	path.push_back(cur_pos);
      }
    } else if (k.first > a.first && k.second < a.second) {
      for (char i = a.first + 1, j = a.second - 1; i < k.first || j > k.second; i++, j--) {
	std::pair<char, char> cur_pos = std::make_pair(i, j);
       	path.push_back(cur_pos);
       }
    } else if (k.first < a.first && k.second > a.second) {
      for (char i = a.first - 1, j = a.second + 1; i > k.first || j < k.second; i--, j++) {
	std::pair<char, char> cur_pos = std::make_pair(i, j);
	path.push_back(cur_pos);
       }
    } else if (k.first < a.first && k.second < a.second) {
      for (char i = a.first - 1, j = a.second - 1; i > k.first || j > k.second; i--, j--) {
	std::pair<char, char> cur_pos = std::make_pair(i, j);
	path.push_back(cur_pos);
      }
    }
  }
  return path;}

bool Chess::in_stalemate(bool white) const {

  std::vector<std::pair<char, char>> v, d;
  std::pair<char, char> p, p2, k;

  
  if(white){
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if(board(p) != NULL && board(p)->is_white() && !(board(p)->to_ascii() == 'K')){
	  p2 = p;
	  for (char a = '8'; a >= '1'; a--) {
	    for (char b = 'A'; b <= 'H'; b++) {
	      if(check_move(p2, std::make_pair(b, a))) {
		return false;
	      }
	    }
	  }
	}
      }
    }
  
    
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if(board(p) != NULL && board(p)->to_ascii() == 'K'){
	  k = p;
	  break;
	}
      }
    }
    for(char i = k.first - 1; i <= k.first + 1; i++){
      for(char j = k.second - 1; j <= k.second + 1; j++){
	if (i < 73 && i > 64 && j > 48 && j < 57){
	  std::pair<char, char> P = std::make_pair(i,j);
	  if(board(P) == NULL)
	    v.push_back(P);
	}
      }
    }
    if (!(v.empty())) {
      for(std::vector<std::pair<char, char>>::iterator it = v.begin();
	  it != v.end();
	  ++it) {
	d = in_danger(*it, white);
	if(d.empty()) {
	  return false;
	}
      }
    } else if (v.empty() && d.empty()) {
      //std::cout << "hey" << std::endl;
      return false;
    }
    return true;
  }
  else{
     for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if(board(p) != NULL && !(board(p)->is_white()) && !(board(p)->to_ascii() == 'k')){
	  p2 = p;
	  for (char a = '8'; a >= '1'; a--) {
	    for (char b = 'A';b <= 'H'; b++) {
	      if(check_move(p2, std::make_pair(b, a))) {
		return false;
	      }
	    }
	  }
	}
      }
     }
     
     for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	p = std::make_pair(c,r);
	if(board(p) != NULL && board(p)->to_ascii() == 'k'){
	  k = p;
	  break;
	}
      }
    }
    for(char i = k.first - 1; i <= k.first + 1; i++){
      for(char j = k.second - 1; j <= k.second + 1; j++){
	if (i < 73 && i > 64 && j > 48 && j < 57){
	  std::pair<char, char> P = std::make_pair(i,j);
	  if(board(P) == NULL)
	    v.push_back(P);
	}
      }
    }
    if (!(v.empty())) {
      for(std::vector<std::pair<char, char>>::iterator it = v.begin();
	  it != v.end();
	  ++it) {
	d = in_danger(*it, white);
	if(d.empty()) {
	  return false;
	}
      }
    } else if (v.empty() && d.empty()) {
      //std::cout << "hey" << std::endl;
      return false;
    }
    return true;
  }
  
  return false;
}



/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<< (std::ostream& os, const Chess& chess) {
	// Write the board out and then either the character 'w' or the character 'b',
	// depending on whose turn it is
	return os << chess.get_board() << (chess.turn_white() ? 'w' : 'b');
}


std::istream& operator>> (std::istream& is, Chess& chess) {
  //Board::Board* Board;
  
  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {
	std::pair<char, char> place = std::make_pair(c, r);
	//std::cout << place.first << " " << place.second << std::endl;
	chess.board.clear_piece(place);
    }
  }
  char read;
  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {
      if (is >> read) {
	std::pair<char, char> place = std::make_pair(c, r);
	chess.board.add_piece(place, read);
      }
    }
  }
  if (is >> read) {
    if (read == 'b') {
      chess.is_white_turn = false;
    }
    if (read == 'w') {
      chess.is_white_turn = true;
    }
  }
  return is;
}
