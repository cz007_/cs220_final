#include "Piece.h"
#include "Queen.h"
#include "moveshape.h"

bool Queen::legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  if (legal_move_shape(start, end)) {
      return true;
    }
    return false;
}
