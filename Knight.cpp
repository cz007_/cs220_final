#include "Knight.h"
#include "Piece.h"

bool Knight::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  
  if (end.first == start.first + 1 && end.second == start.second + 2) {
    return true;
  } else if (end.first == start.first + 2 && end.second == start.second + 1) {
    return true;
  } else if (end.first == start.first + 2 && end.second == start.second - 1) {
    return true;
  } else if (end.first == start.first + 1 && end.second == start.second - 2) {
    return true;
  } else if (end.first == start.first - 1 && end.second == start.second - 2) {
    return true;
  } else if (end.first == start.first - 2 && end.second == start.second - 1) {
    return true;
  } else if (end.first == start.first - 2 && end.second == start.second + 1) {
    return true;
  } else if (end.first == start.first - 1 && end.second == start.second + 2) {
    return true;
  }
  return false;
}

bool Knight::legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  if (legal_move_shape(start, end)) {
    return true;
  } else {
    return false;
  }
}
