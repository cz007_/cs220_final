#ifndef ROOK_H
#define ROOK_H

#include "Piece.h"
#include "moveshape.h"
#include <vector>

class Rook : public Piece {

public:
 //Checks the valid rows and columns, if end is within those bounds, then the move is valid.
  //Otherwise, the move is not valid and will return false.
  ~Rook(){}
  
  bool legal_move_shape(std::pair<char, char> start,
			std::pair<char, char> end) const {
     if (horizontal_vertical(start, end)) {
       return true;
     }
     return false;
   }

  bool legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const;
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	char to_ascii() const {
		return is_white() ? 'R' : 'r';
	}

private:
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	Rook(bool is_white) : Piece(is_white) {}

	friend Piece* create_piece(char piece_designator);
};

#endif // ROOK_H
