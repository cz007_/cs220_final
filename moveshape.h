#ifndef MOVESHAPE_H
#define MOVESHAPE_H
#include <vector>

bool horizontal_vertical(std::pair<char, char> start, std::pair<char, char> end);

bool diagonal(std::pair<char, char> start, std::pair<char, char> end);

#endif
