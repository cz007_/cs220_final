#include <iostream>
#include <utility>
#include <map>
#include "Board.h"
#include "CreatePiece.h"
#include "Terminal.h"

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board(){}

//Board function.
const Piece* Board::operator()(std::pair<char, char> position) const {
  //Checks ascii values of the chars of the pair.
  //First has to be between A and H, second has to be between 1 and 8.
  //If it succeeds, return a const pointer to the Piece at that location.
  if ((position.first < 73) && (position.first > 64) && (position.second < 57) && (position.second > 48)) {
    //Returns the map position.
    if (occ.find(position) != occ.end()) {
      const Piece *cur_piece = occ.at(position);
      return cur_piece;
    }
  } else {
    return NULL;
  }
  return NULL;
}


//Checks if we can add a piece onto the board.
bool Board::add_piece(std::pair<char, char> position, char piece_designator) {
  //Checks ascii values of the chars of the pair.
  if ((position.first < 73) && (position.first > 64) && (position.second < 57) && (position.second > 48)) {
    Piece* piece = create_piece(piece_designator);
    if (piece == nullptr) {
      return false;
    }
    occ[position] = piece;
  }
  return true;
}


// Removes a piece from the board
void Board::clear_piece(std::pair<char, char> p){
  std::map<std::pair<char, char>, Piece*>::iterator it;
  if ((p.first < 73) && (p.first > 64) && (p.second < 57) && (p.second > 48)) {
    it = occ.find(p);
    if (it != occ.end()) {
      if (occ.at(p) != nullptr) {
	delete it->second;//occ.at(p);
	occ.erase(p);
      }
    }
  }
}

//Checks for valid kings.
bool Board::has_valid_kings() const {
  //Variables initizliaed to count the respective number of kings on the board.
  int countKing = 0;
  int countking = 0;
  //Iterates the map to check for king/King.
  for (std::map<std::pair<char, char>, Piece*>::const_iterator it = occ.cbegin();
       it != occ.cend();
       ++it){
    //Calls to_ascii of the piece to get the values.
    if (it->second != nullptr) {
      if (it->second->to_ascii() == 'k') {
	countking++;
      } else if (it->second->to_ascii() == 'K') {
	countKing++;
      }
    }
  }
  //If both count are equal to 1, then return true.
  if (countKing != 1 || countking != 1) {
    return false;
  }
  return true;
}

//Displays the board here.
void Board::display() const {
  std::vector<std::pair<std::pair<char, char>, bool>> board;
  for (char r = '8'; r >= '1'; r--) {
    for (char c = 'A'; c <= 'H'; c++) {
      if ((r % 2 == 0 && c % 2 == 0) || (r % 2 == 1 && c % 2 == 1)) {
	std::pair<char, char> place = std::make_pair(c, r);
	std::pair<std::pair<char, char>, bool> info = std::make_pair(place, true);
	board.push_back(info);
      }
      else {
	std::pair<char, char> place = std::make_pair(c, r);
	std::pair<std::pair<char, char>, bool> info = std::make_pair(place, false);
	board.push_back(info);
      }
    }
  }
  std::cout << std::endl <<  "■   A  B  C  D  E  F  G  H   ■" << std::endl;
  int row1 = 8;
  int row2 = 8;
  for (std::vector<std::pair<std::pair<char, char>, bool>>::const_iterator it = board.cbegin();
       it != board.cend();
       ++it) {
    if ((it->first).first == 'A') {
      std::cout << std::endl;
    }
      if ((it->first).first == 'A') {
	std::cout << row1 << "  ";
      row1--;
    }
    if (it->second == true) {
      Terminal::color_bg(Terminal::WHITE);
    }
    if (it->second == false) {
      Terminal::color_bg(Terminal::BLACK);
    }
    const Piece* piece = nullptr;
    if (occ.count(std::pair<char, char>(it->first.first, it->first.second))) {
      piece = occ.at(std::pair<char, char>(it->first.first, it->first.second));
    }
    if (piece) {
      Terminal::color_fg(true, Terminal::BLACK);
      if (piece->to_ascii() == 'm') {
	std::cout << " m ";
      }
      if (piece->to_ascii() == 'p') {
	std::cout << " \u265f ";
      }
      if (piece->to_ascii() == 'n') {
	std::cout << " \u265e ";
      }
      if (piece->to_ascii() == 'b') {
	std::cout << " \u265d ";
      }
      if (piece->to_ascii() == 'r') {
	std::cout << " \u265c ";
      }
      if (piece->to_ascii() == 'q') {
	std::cout << " \u265b ";
      }
      if (piece->to_ascii() == 'k') {
	std::cout << " \u2654 ";
      }
      Terminal::color_fg(true, Terminal::WHITE);
      if (piece->to_ascii() == 'M') {
	std::cout << " M ";
      }
      if (piece->to_ascii() == 'P') {
	std::cout << " \u2659 ";
      }
      if (piece->to_ascii() == 'N') {
	std::cout << " \u2658 ";
      }
      if (piece->to_ascii() == 'B') {
	std::cout << " \u2657 ";
      }
      if (piece->to_ascii() == 'R') {
	std::cout << " \u2656 ";
      }
      if (piece->to_ascii() == 'Q') {
	std::cout << " \u2655 ";
      }
      if (piece->to_ascii() == 'K') {
	std::cout << " \u265a ";
      }
    }
    else {
      std::cout << "   ";
    }
    Terminal::set_default();
    if ((it->first).first == 'H') {
      std::cout << "  " << row2;
      row2--;
    }
  }
    
  std::cout << std::endl << std::endl;
  std::cout << "■   A  B  C  D  E  F  G  H   ■" << std::endl << std::endl;
}


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<<(std::ostream& os, const Board& board) {
	for(char r = '8'; r >= '1'; r--) {
		for(char c = 'A'; c <= 'H'; c++) {
			const Piece* piece = board(std::pair<char, char>(c, r));
			if (piece) {
				os << piece->to_ascii();
			} else {
			  os << '-';
			}
		}
		os << std::endl;
	}
	return os;
}

