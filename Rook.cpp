#include "Piece.h"
#include "Rook.h"

//Rook's capture shape.
bool Rook::legal_capture_shape(std::pair<char, char> start,
			       std::pair<char, char> end) const {
  if (legal_move_shape(start, end)) {
    return true;
  } else {
    return false;
  }
}
