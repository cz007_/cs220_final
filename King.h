#ifndef KING_H
#define KING_H

#include "Piece.h"
#include <vector>

class King : public Piece {

public:

  ~King(){}
  
  bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const;
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
  bool legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const;
	char to_ascii() const {
		return is_white() ? 'K' : 'k';
	}

private:
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	King(bool is_white) : Piece(is_white) {}

	friend Piece* create_piece(char piece_designator);
};

#endif // KING_H
