#include "Pawn.h"
#include <iostream>
#include <cstdlib>
#include <vector>

bool Pawn::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  //bool s = true;
  std::vector<std::pair<char, char>> pairs;
  if (end.first < 73 && end.first > 64 && end.second > 48 && end.second < 57) {
    pairs.push_back(end);
    // if piece is white and at start
    if (start.second == 50 && is_white()) {
      if((end.second == start.second + 1 || end.second == start.second + 2) && (start.first == end.first) ){
	return true;
      }
      else {
	return false;
	// if piece is white and not at start
      }
    }
    else if(is_white()) {
      if((end.second ==  start.second + 1) && (start.first == end.first) ){
	return true;
      }
      else {
	return false;
	// if piece is black and at start
      }
    }
    else if(start.second == 55 && !is_white()) {
      if((end.second == start.second - 1 || end.second == start.second - 2) && (start.first == end.first) ){
	return true;
      }
      else {
	return false;
	// if piece is black and not at start
      }
    }
    else if(!is_white()) {
      if((end.second == start.second - 1) && (start.first == end.first) ) {
	return true;
      }
      else {
	return false;
      }
    }
    else {
      std::cout << "Error, check code" << std::endl;
      return false;
    }
  }
  return false;
}

bool Pawn::legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  if(is_white()){
    if(start.first == 65){
      if(end.second == start.second + 1 && end.first == start.first + 1)
	return true;
      else
	return false;
    }
    if(start.first == 72){
      if(end.second == start.second + 1 && end.first == start.first - 1)
	return true;
      else
	return false;
    }
    if(end.second == start.second + 1 && (end.first == start.first - 1 || end.first ==  start.first + 1))
      return true;
    else
      return false;
  }
  else if(!is_white()){
    if(start.first == 65){
      if(end.second == start.second - 1 && end.first == start.first + 1)
	return true;
      else
	return false;
    }
    if(start.first == 72){
      if(end.second == start.second - 1 && end.first == start.first - 1)
	return true;
      else
	return false;
    }
    if(end.second == start.second - 1 && (end.first == start.first - 1 || end.first ==  start.first + 1))
      return true;
    else
      return false;
  }
  else
    return false;
}

