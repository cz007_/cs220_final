#include "moveshape.h"
#include <vector>
#include <iterator>
#include <utility>

using std::pair;
using std::vector;
using std::make_pair;

bool horizontal_vertical(pair<char, char> start, pair<char, char> end) {
  vector<pair<char, char>> path;
  //Checks if the end move is in the same row.
  //Then runs a for loop to check whether the move is legal.
  if (end.second == start.second) {
    //Checks left or right of the piece.
    if (end.first > start.first) {
      for (char i = start.first; i <= end.first; i++) {
	//Checks for piece in the way.
	pair<char, char> place = make_pair(i, end.second);
	path.push_back(place);
      }
    } else if (end.first < start.first) {
      for (char i = start.first; i >= end.first; i--) {
	//Checks for piece in the way.
	pair<char, char> place = make_pair(i, end.second);
	path.push_back(place);
      }
    } else {
      return false;
    }
  }  else if (end.first == start.first) {
    //Checks left or right of the piece.
    char diff = start.second - end.second;
    if (start.second < end.second) {
      for (char i = start.second; i <= end.second; i++) {
	//Checks for piece in the way.
	pair<char, char> place = make_pair(end.first, i);
	path.push_back(place);
      }
    } else if (start.second > end.second) {
      for (char i = start.second; i >= end.second; i--) {
	//Checks for piece in the way.
	pair<char, char> place = make_pair(end.first, i);
	path.push_back(place);
      }
    } else {
      return false;
    }
  }
  for (vector<pair<char, char>>::const_iterator it = path.cbegin();
	it != path.cend();
	++it) {
     if (it->first == end.first && it->second == end.second) {
       return true;
     }
   }
   return false;
}

bool diagonal(std::pair<char, char> start, std::pair<char, char> end) {
  vector<pair<char, char>> path;
  //top right
  if (abs(start.first - end.first) == abs(start.second - end.second)){
    if (end.first > start.first && end.second > start.second) {
      for (char i = start.first, j = start.second; i <= end.first || j <= end.second; i++, j++) {
	pair<char, char> place = make_pair(i, j);
	path.push_back(place);
      }
    } else if (end.first > start.first && end.second < start.second) {
      for (char i = start.first, j = start.second; i <= end.first || j >= end.second; i++, j--) {
	pair<char, char> place = make_pair(i, j);
	path.push_back(place);
      }
    } else if (end.first < start.first && end.second > start.second) {
      for (char i = start.first, j = start.second; i >= end.first || j <= end.second; i--, j++) {
	pair<char, char> place = make_pair(i, j);
	path.push_back(place);
      }
    } else if (end.first < start.first && end.second < start.second) {
      for (char i = start.first, j = start.second; i >= end.first || j >= end.second; i--, j--) {
      	pair<char, char> place = make_pair(i, j);
	path.push_back(place);
      }
    }
  }
  for (vector<pair<char, char>>::const_iterator it = path.cbegin();
       it != path.cend();
       ++it) {
    if (it->first == end.first && it->second == end.second) {
      return true;
    }
  }
  return false;
}
  
